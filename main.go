package main

import (
	"github.com/hybridgroup/gobot"
	"github.com/hybridgroup/gobot/platforms/firmata"
	"github.com/hybridgroup/gobot/platforms/gpio"
	"time"
)

// based on http://www.hacktronics.com/Tutorials/arduino-and-7-segment-led.html
type Display struct {
	leda           *gpio.LedDriver
	ledb           *gpio.LedDriver
	ledc           *gpio.LedDriver
	ledd           *gpio.LedDriver
	lede           *gpio.LedDriver
	ledf           *gpio.LedDriver
	ledg           *gpio.LedDriver
	leddot         *gpio.LedDriver


	all            []*gpio.LedDriver
	one            []*gpio.LedDriver
	two            []*gpio.LedDriver
	three          []*gpio.LedDriver
	four           []*gpio.LedDriver
	five           []*gpio.LedDriver
	six            []*gpio.LedDriver
	seven          []*gpio.LedDriver
	eight          []*gpio.LedDriver
	nine           []*gpio.LedDriver
	zero           []*gpio.LedDriver

	adaptor *firmata.FirmataAdaptor
}

func NewDisplay(firmataAdaptor *firmata.FirmataAdaptor) Display {
	d := Display{adaptor: firmataAdaptor}
	d.init()
	return d
}
func (d *Display) init() {
	d.leda = gpio.NewLedDriver(d.adaptor, "led", "2")
	d.ledb = gpio.NewLedDriver(d.adaptor, "led", "3")
	d.ledc = gpio.NewLedDriver(d.adaptor, "led", "4")
	d.ledd = gpio.NewLedDriver(d.adaptor, "led", "5")
	d.lede = gpio.NewLedDriver(d.adaptor, "led", "6")
	d.ledf = gpio.NewLedDriver(d.adaptor, "led", "7")
	d.ledg = gpio.NewLedDriver(d.adaptor, "led", "8")
	d.leddot = gpio.NewLedDriver(d.adaptor, "led", "9")
}

func (d *Display) clear() {
	all := []*gpio.LedDriver{d.leda, d.ledb, d.ledc, d.ledd, d.lede, d.ledf, d.ledg, d.leddot}
	for _, l := range all {
		l.Off()
	}
}

func (d *Display) One() {
	d.clear()
	oneleds := []*gpio.LedDriver{d.ledb, d.ledc}
	for _, l := range oneleds {
		l.Toggle()
	}
}

func (d *Display) Two() {
	d.clear()
	two := []*gpio.LedDriver{d.leda, d.ledb, d.ledg, d.ledd, d.lede}
	for _, l := range two {
		l.Toggle()
	}
}

func (d *Display) Three() {
	d.clear()
	leds := []*gpio.LedDriver{d.leda, d.ledb, d.ledc, d.ledd, d.ledg}
	for _, l := range leds {
		l.Toggle()
	}
}

func (d *Display) Four() {
	d.clear()
	leds := []*gpio.LedDriver{d.ledb, d.ledc, d.ledf, d.ledg}
	for _, l := range leds {
		l.Toggle()
	}
}

func (d *Display) Five() {
	d.clear()
	leds := []*gpio.LedDriver{d.leda, d.ledf, d.ledg, d.ledc, d.ledd}
	for _, l := range leds {
		l.Toggle()
	}
}

func (d *Display) Six() {
	d.clear()
	leds := []*gpio.LedDriver{d.ledc, d.ledd, d.lede, d.ledf, d.ledg}
	for _, l := range leds {
		l.Toggle()
	}
}

func (d *Display) Seven() {
	d.clear()
	leds := []*gpio.LedDriver{d.leda, d.ledb, d.ledc}
	for _, l := range leds {
		l.Toggle()
	}
}

func (d *Display) Eight() {
	d.clear()
	leds := []*gpio.LedDriver{d.leda, d.ledb, d.ledc, d.ledd, d.lede, d.ledf, d.ledg}
	for _, l := range leds {
		l.Toggle()
	}
}

func (d *Display) Nine() {
	d.clear()
	leds := []*gpio.LedDriver{d.leda, d.ledb, d.ledc, d.ledf, d.ledg}
	for _, l := range leds {
		l.Toggle()
	}
}

func (d *Display) Zero() {
	d.clear()
	leds := []*gpio.LedDriver{d.leda, d.ledb, d.ledc, d.ledd, d.lede, d.ledf}
	for _, l := range leds {
		l.Toggle()
	}
}

func main() {
	gbot := gobot.NewGobot()
	firmataAdaptor := firmata.NewFirmataAdaptor("arduino", "/dev/tty.usbmodem1411")

	display := NewDisplay(firmataAdaptor)

	work := func() {
//		gobot.Every(20 * time.Second, func() {
			display.Zero()
			time.Sleep(1 * time.Second)
			display.One()
			time.Sleep(1 * time.Second)
			display.Two()
			time.Sleep(1 * time.Second)
			display.Three()
			time.Sleep(1 * time.Second)
			display.Four()
			time.Sleep(1 * time.Second)
			display.Five()
			time.Sleep(1 * time.Second)
			display.Six()
			time.Sleep(1 * time.Second)
			display.Seven()
			time.Sleep(1 * time.Second)
			display.Eight()
			time.Sleep(1 * time.Second)
			display.Nine()
//		})
	}

	robot := gobot.NewRobot("bot",
		[]gobot.Connection{firmataAdaptor},
		display.all,
		work,
	)

	gbot.AddRobot(robot)

	gbot.Start()
}

